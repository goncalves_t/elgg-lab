# Disclaimer!

This work was made on the goal of build a VM for the Hublinked project.

**Please, do not use this on production or for a public usage. All the passwords are written in the scripts and there is a default vagrant ssh key.**

# Requirements 

View: https://gitlab.com/goncalves_t/elgg-lab#requirements-work-in-progress

# Usage

```bash
cd ~
git clone https://gitlab.com/goncalves_t/elgg-lab/
cd elgg-lab/automated/
./start.sh
```

You can now go to the ip address (for exemple http://192.168.183.128/) with your favorite web browser to access at the installation page of Elgg.

You can also connect to the CLI of the VM with ```vagrant ssh```
or ```ssh vagrant@ipaddressvm```. 

## Access to Elgg

You can access to elgg admin panel with the following credentials:
 - Login: ```admin```
 - Password: ```vagrant```

# Video

[![asciicast](https://asciinema.org/a/TqudYdfRJeelZcPz5GDC3z7Jk.svg)](https://asciinema.org/a/TqudYdfRJeelZcPz5GDC3z7Jk)

Recording available at: https://asciinema.org/a/TqudYdfRJeelZcPz5GDC3z7Jk
