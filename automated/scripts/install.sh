#!/bin/bash

echo "This script will install and configure Apache2, Mariadb-Server and Elgg 2.3.9"

# Install PHP5.6 & Apache2.4 & MariaDB
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
sudo apt update && sudo apt install -y php5 php5-intl php5-gd php5-mysql apache2 mariadb-server unzip

# Initialize the database

MYSQL_PASS="vagrant"
mysqladmin -u root password "$MYSQL_PASS"
mysql -u root -p"$MYSQL_PASS" -e "UPDATE mysql.user SET Password=PASSWORD('$MYSQL_PASS') WHERE User='root'"

# Populate the database
mysql -u root -p"$MYSQL_PASS" -e "CREATE DATABASE elggdb"
mysql -u root -p"$MYSQL_PASS" -e "CREATE USER 'elgg'@'localhost' IDENTIFIED BY 'vagrant'"
mysql -u root -p"$MYSQL_PASS" -e "GRANT ALL ON elggdb.* TO 'elgg'@'localhost' IDENTIFIED BY 'vagrant' WITH GRANT OPTION"
mysql -u root -p"$MYSQL_PASS" -e "FLUSH PRIVILEGES"

# Create elgg.conf
cat <<EOF> /home/vagrant/elgg.conf
<VirtualHost *:80>
ServerAdmin admin@example.com
DocumentRoot /var/www/html/elgg/
ServerName example.com
<Directory /var/www/html/elgg/>
Options FollowSymLinks
AllowOverride All
</Directory>
ErrorLog /var/log/apache2/elgg-error_log
CustomLog /var/log/apache2/elgg-access_log common
</VirtualHost>
EOF
sudo cp /home/vagrant/elgg.conf /etc/apache2/sites-available/elgg.conf

# Configure Apache
sudo a2ensite elgg
sudo a2enmod rewrite
sudo a2dissite 000-default
service apache2 restart

# Download and configure Elgg v2.3.9
cd /tmp
wget https://elgg.org/download/elgg-2.3.9.zip
unzip elgg-2.3.9.zip
sudo cp -r elgg-2.3.9 /var/www/html/elgg
sudo mkdir /var/www/html/elgg/data
sudo chown -R www-data:www-data /var/www/html/elgg/
sudo chmod -R 755 /var/www/html/elgg/

# Create a variable that contains private IP
ip="$(sudo ifconfig eth1 | sed -n '/inet addr:/s/ *inet addr:\([[:digit:].]*\) .*/\1/p')"

# Import database
mysql -u root -p"$MYSQL_PASS" elggdb < /tmp/elggdb.sql
mysql -u root -p"$MYSQL_PASS" elggdb -e 'update elgg_sites_entity set url="'http://$ip/'";'

# Import Elgg configuration file
cp /tmp/settings.php /var/www/html/elgg/elgg-config/
service apache2 restart

# Display IP address
echo "You can connect with a ssh vagrant@$ip. To access to Elgg, go to http://$ip/"
