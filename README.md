Experiments with packer and vagrant to build an Elgg VM.

# Disclaimer!
This work was made with the goal of building a VM for the Hublinked project.

**Please, do not use this on production or for a public usage. All the passwords are written in the scripts and there is a default vagrant ssh key.**

# Goal
The aim is to create a VM for an educational use, based on specific
characteristics.

The Git repository can create a VM which contains:
- Debian 8.11.0
- Apache v2.4.10
- MariaDB Server
- PHP v5.6
- Elgg v2.3.9


# Requirements (work in progress)
In theory, it works on all OS supported by Vagrant, Packer and Virtualbox (all three).

All releases are available at:
- Vagrant: https://releases.hashicorp.com/vagrant/
- Packer: https://releases.hashicorp.com/packer/
- Virtualbox and Virtualbox extension: https://www.virtualbox.org/wiki/Download_Old_Builds_5_2

We are making test on the following OS: 
 
## Debian 9.9
- Vagrant: version >= 2.2.3 at https://releases.hashicorp.com/vagrant/2.2.3/vagrant_2.2.3_x86_64.deb
- Packer: version >= 1.3.5 at https://releases.hashicorp.com/packer/1.3.5/packer_1.3.5_linux_amd64.zip
- Virtualbox: version 5.2.30 at https://download.virtualbox.org/virtualbox/5.2.30/virtualbox-5.2_5.2.30-130521~Debian~stretch_amd64.deb
- Virtualbox extension: version 5.2.30 at https://download.virtualbox.org/virtualbox/5.2.30/Oracle_VM_VirtualBox_Extension_Pack-5.2.30.vbox-extpack

## MacOS 
- Vagrant: version >= 2.2.3 at https://releases.hashicorp.com/vagrant/2.2.3/vagrant_2.2.3_x86_64.dmg
- Packer: version >= 1.3.5 at https://releases.hashicorp.com/packer/1.3.5/packer_1.3.5_darwin_amd64.zip
- Virtualbox: version 5.2.30 at https://download.virtualbox.org/virtualbox/5.2.30/VirtualBox-5.2.30-130521-OSX.dmg
- Virtualbox extension: version 5.2.30 at https://download.virtualbox.org/virtualbox/5.2.30/Oracle_VM_VirtualBox_Extension_Pack-5.2.30.vbox-extpack


# Usage
 You can choose between two options:
 - [Solution 1][1]: manually build a packer box and launch vagrant from this box
 - [Solution 2][2]: execute a fully automated script

## Solution 1: manually
The instructions to build and run the VM are contained in the ```manually/``` folder.

## Solution 2: automated (only available on Unix systems)
The instructions to build and run the VM are contained in the ```automated/``` folder.

# Managing the Vagrant VM
Once the box was built with packer, it may not be necessary to
re-build it again later.

You need to use Vagrant to manage the VM. Few examples below:
 - Start the VM: ```vagrant up```
 - Connect to the VM: ```vagrant ssh```
 - Stop the VM: ```vagrant halt```
 - Resume the VM: ```vagrant up```
 - Delete the VM: ```vagrant destroy -f```

# Specs
These characteristics are the same with the both solutions.
- Processor: 1 vcpu
- Memory: 512 Mo
- Disk size: 10 Go
- Network adaptaters: eth0 (NAT) and eth1 (Virtualbox private network)

# Sources

## Installation
- Vagrant for Debian: https://linuxize.com/post/how-to-install-vagrant-on-debian-9/?query=packer
- Packer for Debian: https://www.packer.io/intro/getting-started/install.html

## Inspiration
- Packer: https://github.com/deimosfr/packer-debian
- Elgg v2.3.9: https://www.howtoforge.com/tutorial/ubuntu-elgg-social-network-installation/


[1]: https://gitlab.com/goncalves_t/elgg-lab/tree/master/manually
[2]: https://gitlab.com/goncalves_t/elgg-lab/tree/master/automated
