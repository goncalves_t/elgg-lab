# Disclaimer!
This work was made on the goal of build a VM for the Hublinked project.

**Please, do not use this on production or for a public usage. All the passwords are written in the scripts and there is a default vagrant ssh key.**

# Requirements 
View: https://gitlab.com/goncalves_t/elgg-lab#requirements-work-in-progress

# Usage
```bash
cd ~
git clone https://gitlab.com/goncalves_t/elgg-lab/
cd elgg-lab/manually/
packer build Elgg-debian8.json
vagrant up
```

You can now go to the ip address (for exemple http://192.168.183.128/) with your favorite web browser to access at the installation page of Elgg.

You can also connect to the CLI of the VM with ```vagrant ssh```
or ```ssh vagrant@ipaddressvm```. 

## First access to Elgg
Please, enter the following informations to configure and access to Elgg:
- Database installation
	- Database Username: ```elgg```
    - Database Password: ```vagrant```
    - Database Name: ```elggdb```
- Configure site
    - Data Directory: ```/var/www/html/elgg/data```
- Create admin account
    - Display Name: ```admin```
    - Email Address: ```admin@example.com```
    - Password: Password of your choice, type twice

# Videos
## Packer build
[![asciicast](https://asciinema.org/a/GdC5pcnNp3PHJ2BHkOpAaFZYE.svg)](https://asciinema.org/a/GdC5pcnNp3PHJ2BHkOpAaFZYE)

Recording available at: https://asciinema.org/a/GdC5pcnNp3PHJ2BHkOpAaFZYE

## Vagrant up
[![asciicast](https://asciinema.org/a/OqCfCwq3nP80hTBTYzH5DATxI.svg)](https://asciinema.org/a/OqCfCwq3nP80hTBTYzH5DATxI)

Recording available at: [https://asciinema.org/a/OqCfCwq3nP80hTBTYzH5DATxI](https://asciinema.org/a/OqCfCwq3nP80hTBTYzH5DATxI)
